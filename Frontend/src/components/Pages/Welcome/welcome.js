import React from 'react';
import PropTypes from 'prop-types';

const Welcome = (props) => {
  const show = props.show;
  return (
    <>
      <h1>Welcome</h1>
      { show ? <p>Maybe you will see this?</p> : null }
    </>
  );
};

Welcome.propTypes = {
  show: PropTypes.bool.isRequired,
};

export default Welcome;
