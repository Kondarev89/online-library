import React, { useState, useEffect, useContext } from "react";
import { BASE_URL } from "../../../common/constants";
import UserContext, { getLoggedUser } from "../../../providers/UserContext";
import Button from "react-bootstrap/Button";
import "./Details.css";
import { Col } from "react-bootstrap";

const Details = (props) => {
  const history = props.history;
  const [user, setUser] = useState(getLoggedUser());
  const [error, setError] = useState(null);

  const [book, setBook] = useState({
    id: "",
    title: "",
    isBorrowed: false,
    reviews: [],
    rates: [],
  });

  const [reviews, setReviews] = useState({
    reviews: "",
    title: "",
    urlPoster: null,
  });

  const id = props.match.params.id;
  const userContext = useContext(UserContext);
  const loggedUser = userContext.user;


  useEffect(() => {
    fetch(`${BASE_URL}/books/${id}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
      },
    })
      .then((b) => b.json())
      .then((b) => {
        if (b.error) {
          throw new Error(b.message);
        }

        setBook(b);

      })
      .catch((error) => setError(error.message));

  }, [id]);

  useEffect(() => {
    fetch(`${BASE_URL}/books/${id}/reviews`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
      },
    })
      .then((r) => r.json())
      .then((r) => {
        if (r.error) {
          throw new Error(r.message);
        }

        setReviews(r);
      })
      .catch((error) => setError(error.message));
  }, []);

  const rev = reviews.reviews;
  // console.log(rev);

  const deleteReview = (revId) => {
    fetch(`${BASE_URL}/books/${id}/reviews/${revId}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
      },
    })
      .then((r) => r.json())
      .then((r) => {
        if (r.error) {
          throw new Error(r.message);
        }
        setReviews(r);
      })
      .catch((error) => setError(error.message));
    const newReview = rev.filter((r) => r.id !== id);
    setReviews(newReview);
  };


  return (
    <Col sm="2" className="Sidebar">
      <div >

        <h1>
          {book.title}
          <img src={book.urlPoster} alt={book.title} width="333" height="333" />
          <Button className="btn-dark" onClick={() => history.push(`/books/${book.id}/reviews`)}>
            Create review
          </Button>
          <Button className="btn-dark" onClick={() => history.goBack()}>Back</Button>
        </h1>
        <h2 key="id"> Reviews:</h2>
        <h3>
          {user.id === loggedUser.id ? (  ///can't do it....
            rev
            ? rev.map((r) => (<p> {r.content}
              <Button className="btn-dark" onClick={() => deleteReview(r.id)}>Delete review</Button>
              <Button className="btn-dark" onClick={() => history.push(`/books/${book.id}/reviews/${r.id}`)}>Edit review</Button>
              <Button className="btn-dark" onClick={() => history.push(`/books/reviews/${r.id}/votes`)}>Like</Button></p>
            ))
            : null ) : (
            <h1>
          <Button className="btn-dark" onClick={() => history.push(`/books/${book.id}/reviews`)}>
            Create review
          </Button>
          <Button className="btn-dark" onClick={() => history.goBack()}>Back</Button>
        </h1>
          )}
        </h3>
      </div>
    </Col>
  );
};

export default Details;
//


const counter = () => {
  const [count,setCount] = useState(0);
  return (
    <div>
      <button onClick={() => setCount(count + 1)}>Count:{counter}</button>
    </div>
  )
}