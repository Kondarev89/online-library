import { ReturnBookDTO } from 'src/dtos/return.book.dto';
import { BorrowedBookLog } from 'src/models/boroweBook.entity';
import { User } from './../../models/user.entity';
import { Book } from './../../models/book.entity';
import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TransformServices } from '../transform.services';
import { Review } from 'src/models/review.entity';

@Injectable()
export class BooksService {
  private static readonly READING_POINTS_FOR_RETURNED_BOOK = 1;

  constructor(
    private readonly transform: TransformServices,

    @InjectRepository(Book) private readonly bookRepository: Repository<Book>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(BorrowedBookLog)
    private readonly borrowBookRepository: Repository<BorrowedBookLog>,
    @InjectRepository(Review)
    private readonly reviewRepository: Repository<Review>,
  ) {}

  async getAllBooks(pageSize: number, page: number): Promise<ReturnBookDTO[]> {
    const books = await this.bookRepository.find({
      skip: (page - 1) * pageSize,
      take: pageSize, 
      relations: ['rates', 'reviews']
   });
    return books
  }
  async getBookById(id: number): Promise<Book> {
    const book = await this.bookRepository.findOne({
     where: {id} ,
     relations: ['rates', 'reviews']
    });
    return book;
  }
  async borrowBook(bookId: number, userId: number): Promise<Book> {
    const book = await this.bookRepository.findOne({
      where: {
        id: bookId,
      },
      relations: ['users'],
    });

    if (!book) {
      throw new NotFoundException('Invalid Book Id!');
    }
    const user = await this.userRepository.findOne(userId);
    if (!user) {
      throw new NotFoundException('Invalid user Id!');
    }

    if (book.isBorrowed) {
      throw new BadRequestException('The book is already borrowed!');
    }

    book.isBorrowed = true;

    const borrowTheBook = await this.borrowBookRepository.findOne({
      where: {
        book: bookId,
        user: userId,
      },
    });
    if (!borrowTheBook) {
      const borrowLog = new BorrowedBookLog();
      borrowLog.book = book;
      borrowLog.user = user;
      await this.borrowBookRepository.save(borrowLog);
    }
    borrowTheBook.hasReturned = false;
    await this.borrowBookRepository.save(borrowTheBook);

    return this.bookRepository.save(book);
  }

  public async returnBook(
    bookId: number,
    userId: number,
  ): Promise<BorrowedBookLog> {
    const bookToBeReturned = await this.bookRepository.findOne({
      where: {
        id: bookId,
      },
      relations: ['borrowedBooks'],
    });
    console.log(bookToBeReturned);
    if (!bookToBeReturned) {
      throw new NotFoundException(`Not a valid book to return!!!`);
    }

    bookToBeReturned.isBorrowed = false;

    const user = await this.userRepository.findOne(userId);

    await this.bookRepository.save(bookToBeReturned);

    const borrowBookLog = await this.borrowBookRepository.findOne({
      where: {
        book: bookId,
        user: userId,
      },
    });
    borrowBookLog.hasReturned = true;

    await this.borrowBookRepository.save(borrowBookLog);

    // TODO: Give reading points also when the user writes a review or when his review gets liked.
    // TODO: Subtract reading points when the user gets banned
    // TODO: Add the reading points to the UserDto
    user.readingPoints += BooksService.READING_POINTS_FOR_RETURNED_BOOK;
    await this.userRepository.save(user);

    return borrowBookLog;
  }

  async createBook(title: string): Promise<Book> {
    const checkBook = await this.bookRepository.findOne({
      where: {
        title: title,
      },
    });
    if (checkBook) {
      throw new BadRequestException('This book already exist!');
    }
    const newBook = new Book();
    newBook.title = title;
    return this.bookRepository.save(newBook);
  }
  async deleteBook(bookId: number) {
    const book = await this.bookRepository.delete(bookId);
    return book;
  }
}
