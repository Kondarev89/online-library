import React, { useState, useContext } from "react";
import Button from "react-bootstrap/Button";
import { BASE_URL } from "../../../common/constants";
import { withRouter } from "react-router-dom";
import RateBooks from "./RateBooks";
import "./SingleBook.css";
import "../Card.css"
import ReadingPointsContext from "../../../providers/ReadingPointsContext";

const SingleBook = ({ history, book, borrowHandler, returnHandler }) => {
  const [error, setError] = useState(null);
  const [isBorrowed, setbook] = useState(book.isBorrowed);
  const {readingPoints, setReadingPoints}= useContext(ReadingPointsContext)


  const avgRating = (rates) => {
    const ratesLength = rates.length;
    const reducer = (accumulator, rates) => {
      return accumulator + rates.rate;
    };
    const ratesAvg = rates.reduce(reducer, 0) / ratesLength;
    return Math.round(ratesAvg);
  };

  // console.log(book, avgRating(book.rates))
  return (
    <div className="card">
      <div className="bookMainRow">
        <img src={book.urlPoster} alt={book.title} />
        <div className="bookTitle">{book.title}</div>
        <div className="bookActions">
          {!book.isBorrowed ? (
            <Button variant="dark" onClick={() => borrowHandler(book.id)}>
              Borrow
            </Button>
          ) : (
            <Button variant="info" onClick={() => returnHandler(book.id)}>
              Return book
            </Button>
          )}
          <Button onClick={() => history.push(`/books/${book.id}`)}>Details</Button>
        </div>
      </div>
      <RateBooks bookId={book.id} rate={avgRating(book.rates)} />
    </div>
  );
};

export default withRouter(SingleBook);
