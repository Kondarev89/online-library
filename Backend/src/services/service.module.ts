import { Rate } from './../models/rate.entity';
import { AuthService } from './users/auth.service.';
import { JwtStrategy } from './strategy/jwt.strategy';
import { jwtConstants } from './../constant/secret';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { ReviewsService } from './books/review.services';
import { Book } from './../models/book.entity';
import { Review } from './../models/review.entity';
import { UsersService } from './users/users.services';
import { User } from './../models/user.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BooksService } from './books/books.service';
import { Module } from '@nestjs/common';
import { TransformServices } from './transform.services';
import { BorrowedBookLog } from 'src/models/boroweBook.entity';
import { Token } from 'src/models/token.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User,
      Review,
      Book,
      BorrowedBookLog,
      Rate,
      Token,
    ]),
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: {
        expiresIn: '7d',
      },
    }),
  ],
  controllers: [],
  providers: [
    BooksService,
    UsersService,
    ReviewsService,
    TransformServices,
    JwtStrategy,
    AuthService,
  ],
  exports: [
    BooksService,
    UsersService,
    ReviewsService,
    AuthService,
    TransformServices,
  ],
})
export class ServicesModule {}
