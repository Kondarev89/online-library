import { ReturnBookDTO } from 'src/dtos/return.book.dto';
import { BorrowedBookLog } from './../../models/boroweBook.entity';
import { Book } from './../../models/book.entity';
import { BooksService } from './../../services/books/books.service';
import {
  Controller,
  Param,
  Get,
  Delete,
  Put,
  UseGuards,
  Query,
} from '@nestjs/common';
import { TransformServices } from 'src/services/transform.services';
import { UserId } from 'src/auth/user-id.decorator';
import { AuthGuard } from '@nestjs/passport';

@Controller('books')
export class BooksController {
  constructor(
    private readonly bookService: BooksService,
    private readonly transform: TransformServices,
  ) {}
  @Get()
  @UseGuards(AuthGuard('jwt'))
  async allBooks(
    @Query('pageSize') pageSize: number,
    @Query('page') page: number,
  ): Promise<ReturnBookDTO[]> {
    const allBooks = await this.bookService.getAllBooks(pageSize, page);
    return allBooks;
  }

  @Get(':id')
  @UseGuards(AuthGuard('jwt'))
  async getBookById(@Param('id') id: number): Promise<Book> {
    const book = await this.bookService.getBookById(id);

    return book;
  }

  @Put(':bookId')
  @UseGuards(AuthGuard('jwt'))
  async borrowBook(@Param('bookId') bookId: string, @UserId() userId: number) {
    const borrowedBook = await this.bookService.borrowBook(+bookId, userId);
    return this.transform.transformBorrowBook(borrowedBook);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'))
  async returnBook(
    @Param('id') bookId: number,
    @UserId() userId: number,
  ): Promise<BorrowedBookLog> {
    const returnBook = await this.bookService.returnBook(bookId, userId);
    return returnBook;
  }
}
