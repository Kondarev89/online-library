import { UserRole } from './../../enum/user.role';
import { RolesGuard } from './../../auth/roles.guard';
import { CreateReviewDTO } from './../../dtos/create.review.dto';
import { ReturnReviewDto } from './../../dtos/return.review.dto';
import { ReviewsService } from './../../services/books/review.services';
import {
  Controller,
  Param,
  Post,
  Body,
  Put,
  Get,
  Delete,
  ValidationPipe,
  UseGuards,
} from '@nestjs/common';
import { Review } from 'src/models/review.entity';
import { CreateRateDTO } from 'src/dtos/create.rate.dto';
import { AuthGuard } from '@nestjs/passport';
import { UserId } from 'src/auth/user-id.decorator';
import { Book } from 'src/models/book.entity';

@Controller('books')
export class ReviewController {
  constructor(private readonly reviewService: ReviewsService) {}

  @Post('/:bookId/reviews')
  @UseGuards(AuthGuard('jwt'))
  public async createReview(
    @Param('bookId') bookId: string,
    @UserId() userId: number,
    @Body(new ValidationPipe({ whitelist: true }))
    reviewDetails: CreateReviewDTO,
  ): Promise<ReturnReviewDto> {
    return await this.reviewService.createReview(
      +bookId,
      +userId,
      reviewDetails.content,
    );
  }

  @Put(':bookId/rates')
  @UseGuards(AuthGuard('jwt'))
  async rateBook(
    @Param('bookId') bookId: string,
    @UserId() userId: number,
    @Body(new ValidationPipe({ whitelist: true })) rateDetails: CreateRateDTO,
  ) {
    // TODO: Go through all files and format them
    return this.reviewService.rateBook(+bookId, +userId, rateDetails.rate);
  }

  @Put(':bookId/reviews/:reviewId')
  @UseGuards(AuthGuard('jwt'))

  // TODO: Rename body on all places
  async updateReview(
    @Param('bookId') bookId: string,
    @Param('reviewId') reviewId: string,
    @Body(new ValidationPipe({ whitelist: true }))
    reviewDetails: CreateReviewDTO,
    @UserId() userId,
  ) {
    return this.reviewService.updateReview(
      +bookId,
      +reviewId,
      reviewDetails.content,
      userId,
    );
  }
  
  @Get(':id/reviews')

  async all(@Param('id') id: number): Promise<Book> {
    return  this.reviewService.getAll(id);
  }
  
  
  @Delete(':id/reviews/:reviewId')
  // @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  async deleteReview(@Param('reviewId') reviewId: number): Promise<Review> {
  
    return await this.reviewService.deleteReview(reviewId);
  }

  @Put('reviews/:id/votes')
  @UseGuards(AuthGuard('jwt'))
  async likeDislike(@Param('id') reviewId: number): Promise<any> {
    return this.reviewService.likeDislike(reviewId);
  }
}
