import { LoginUserDTO } from './../../dtos/login.user.dto';
import { AuthService } from './../../services/users/auth.service.';
import { Controller, Post, Body, ValidationPipe, Delete } from '@nestjs/common';
import { GetToken } from 'src/auth/get-token.decorator';

@Controller('session')
export class AuthController {
  constructor(private readonly authServise: AuthService) {}

  @Post()
  async login(
    @Body(new ValidationPipe({ whitelist: true })) body: LoginUserDTO,
  ): Promise<{ token: string }> {
    return await this.authServise.login(body.username, body.password);
  }

  @Delete()
  async logout(@GetToken() token: string): Promise<{ message: string }> {
    await this.authServise.blacklist(token?.slice(7));
    return {
      message: 'You have been logged out!',
    };
  }
}
