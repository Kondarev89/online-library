import React, {  useContext } from 'react';
import UserContext from '../../../providers/UserContext'
import { Button } from 'react-bootstrap'
import { withRouter } from 'react-router-dom';
import "./Home.css"



const Home = (props) => {
  const history = props.history;

  const userContext = useContext(UserContext);

  return (
    <>
      <h1>Welcome</h1>
    </>
  );
}

export default withRouter(Home);






