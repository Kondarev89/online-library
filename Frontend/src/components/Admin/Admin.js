import React from "react";
import "../../App.css";
import { Nav } from "react-bootstrap";
import { withRouter, Switch, Route } from "react-router-dom";
import "./Admin.css";
import BooksAdmin from "./BooksAdmin";
import UsersAdmin from "./UsersAdmin";

const Admin = ({ history }) => {
  return (
    <div>
      <div>Admin Section</div>
      <div className="nav nav-tabs">
        <Nav.Link onClick={() => history.push("/admin/books")}>Books</Nav.Link>
        <Nav.Link onClick={() => history.push("/admin/users")}>Users</Nav.Link>
      </div>
      <Switch>
        <Route path="/admin/books" exact component={BooksAdmin} />
        <Route path="/admin/users" component={UsersAdmin} />
      </Switch>
    </div>
  );
};

export default withRouter(Admin);
