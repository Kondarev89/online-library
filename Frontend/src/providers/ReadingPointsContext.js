import { createContext } from 'react';

const ReadingPointsContext = createContext({
  readingPoints: null,
  setReadingPoints: () => {},
});

export default ReadingPointsContext;