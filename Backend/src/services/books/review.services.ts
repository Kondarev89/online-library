import { Rate } from './../../models/rate.entity';
import { BorrowedBookLog } from './../../models/boroweBook.entity';
import { ReturnReviewDto } from './../../dtos/return.review.dto';
import { User } from './../../models/user.entity';
import { Book } from './../../models/book.entity';
import { Review } from './../../models/review.entity';
import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TransformServices } from '../transform.services';

@Injectable()
export class ReviewsService {
  constructor(
    private readonly transform: TransformServices,
    @InjectRepository(Book) private readonly bookRepository: Repository<Book>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Review)
    private readonly reviewRepository: Repository<Review>,
    @InjectRepository(BorrowedBookLog)
    private readonly borrowedBooksLogRepository: Repository<BorrowedBookLog>,
    @InjectRepository(Rate) private readonly rateRepository: Repository<Rate>,
  ) {}
  async createReview(
    bookId: number,
    userId: number,
    reviewContent: string,
  ): Promise<ReturnReviewDto> {
    const book = await this.bookRepository.findOne(bookId);
    if (!book) {
      throw new NotFoundException('Invalid bookId!');
    }
    const user = await this.userRepository.findOne(userId);
    if (!user) {
      throw new NotFoundException('Invalid userId!');
    }
    const review = new Review();
    review.content = reviewContent;
    review.book = book;
    review.user = user;
    const savedReview = await this.reviewRepository.save(review);
    return this.transform.transformReview(savedReview);
  }

  async updateReview(
    bookId: number,
    reviewId: number,
    reviewContent: string,
    userId: number,
  ) {
    const book = (
      await this.bookRepository.find({
        where: { id: bookId },
        relations: ['reviews'],
      })
    )[0];
    // console.log(foundBook);
    if (!book) {
      throw new NotFoundException('Invalid bookId!');
    }

    const reviewToUpdate = await this.reviewRepository.findOne({
      where: {
        id: reviewId,
      },
      relations: ['user', 'book'],
    });

    const user = await this.userRepository.findOne(userId);
    if (!(user.id === reviewToUpdate.user.id)) {
      throw new BadRequestException('Not a review from this user');
    }

    reviewToUpdate.content = reviewContent;

    const updatedReview = await this.reviewRepository.save(reviewToUpdate);
    console.log(updatedReview);
    return this.transform.transformReview(updatedReview);
  }

  async getAll(id: number): Promise<Book> {
    return await this.bookRepository
      .createQueryBuilder('book')
      .innerJoinAndSelect('book.reviews', 'reviews', 'reviews.isDeleted = :isDeleted', { isDeleted: false })
      .where('book.id = :id', { id })
      .getOne();
  }
  public async deleteReview(reviewId: number): Promise<Review> {
    const deleteReview = await this.reviewRepository.findOne(reviewId);

    if (!deleteReview) {
      throw new NotFoundException(`Invalid review deletion!!!`);
    } else {
      return await this.reviewRepository.save({
        ...deleteReview,
        isDeleted: true,
      });
    }
  }

  async rateBook(bookId: number, userId, rate: number) {
    const borrowLog = await this.borrowedBooksLogRepository.findOne({
      where: {
        user: userId,
        book: bookId,
      },
      relations: ['user', 'book'],
    });

    if (!borrowLog) {
      throw new BadRequestException(
        'The user has not returned the book yet or not borrow never',
      );
    }

    if (!borrowLog.hasReturned) {
      throw new BadRequestException('The user has not returned the book yet!');
    }

    const review = await this.reviewRepository.findOne({
      where: {
        user: userId,
        book: bookId,
      },
    });

    // if (!review) {
    //   throw new BadRequestException("The user doesn't have a review");
    // }

    const user = await this.userRepository.findOne(userId);
    const book = await this.bookRepository.findOne(bookId);
    console.log( "Noooow"+user.rates)
    if (!user && user.rates.some(e => e.book.id === bookId)) {
     throw new BadRequestException("This user already rated this book!")
    } 
    // review.rate = rate;
    const rateEntity = new Rate();
    // rateEntity.review = review;
    rateEntity.rate = rate;
    rateEntity.book = book;
    rateEntity.user = user;

    // TODO: Transform to DTO
    await this.rateRepository.save(rateEntity);
    return rateEntity;
  }
  // async LikeDislike(id: number): Promise<Review> {
  //   return await this.reviewRepository
  //     .createQueryBuilder('reviews')
  //     .innerJoinAndSelect('reviews.user', 'reviews', 'reviews.user = username', { username: '' })
  //     .where('reviews.id = :id', { id })
  //     .getOne();
  // }
  async likeDislike(reviewId: number) {
    const review = await this.reviewRepository.findOne({
      where: {
        id: reviewId,
      },
      relations: ['likes', 'user'],
    });
    if (!review) {
      throw new Error('Invalid review Id!!!');
    }
    return review;
  }
}


