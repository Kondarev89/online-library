import { User } from 'src/models/user.entity';
import { Book } from 'src/models/book.entity';
import { JoinColumn, ManyToOne } from 'typeorm';
import { Review } from './review.entity';
import { PrimaryGeneratedColumn, OneToOne } from 'typeorm';
import { Column, Entity } from 'typeorm';

@Entity('rates')
export class Rate {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  rate: number;

  @JoinColumn()
  @OneToOne(
    () => Review,
    review => review.rate,
  )
  review: Review;

  @ManyToOne(
    () => Book,
    book => book.rates,
  )
  book: Book;

  @ManyToOne(
    () => User,
    user => user.rates,
  )
  user: User;
}
