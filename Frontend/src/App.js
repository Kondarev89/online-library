import React, { useState } from "react";
import "./App.css";
import Home from "./components/Pages/Home/Home";
import Navigation from "./components/Navigation/Navigation";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import UserContext, { getLoggedUser } from "./providers/UserContext";
import SignIn from "./components/Sighin/Signin";
import Header from "./components/Base/Header/Header";
import AllBooks from "./components/Base/Books/AllBooks";
import { Container, Row, Col } from "react-bootstrap";
import Sidebar from "./components/Base/Sidebar/Sidebar";
import Details from "./components/Base/Books/Details";
import CreateReviews from "./components/Base/Reviews/CreateReviews";
import Donations from "./components/Pages/Home/Donations";
import AboutUs from "./components/Pages/Home/AboutUs";
import jwtDecode from "jwt-decode";
import { BASE_URL } from "./common/constants";
import EditReviews from './components/Base/Reviews/EditReviews';
import Admin from "./components/Admin/Admin";
import BooksAdmin from "./components/Admin/BooksAdmin";
import UsersAdmin from "./components/Admin/UsersAdmin";
import ReadingPointsContext from "./providers/ReadingPointsContext";


const App = () => {
  const [user, setUser] = useState(getLoggedUser());
  const [readingPoints, setReadingPoints] = useState(0);

  let payload = "";
  let loggedUser = "";
  const token = localStorage.getItem("token");
  let role = '';
  if (token) {
    payload = jwtDecode(token);
    loggedUser = payload.username;
    role = payload.role;
  

    fetch(`${BASE_URL}/users/${loggedUser}`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token || ""}`,
      },
    })
      .then((r) => r.json())
      .then((data) => {
        if (data.error) {
          throw new Error(data.message);
        }
        setReadingPoints(data[0].readingPoints)
      })
      .catch((error) => error.message);
  }
 

  return (
    <BrowserRouter>
      <UserContext.Provider value={{ user, setUser }}>
        <ReadingPointsContext.Provider value={{readingPoints, setReadingPoints}}>
        <Header loggedUser={loggedUser} readingPoints={readingPoints} role={role}/>
        <div className="mainContainer">
          <Navigation />
          <div className="pageContent">
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/login" component={SignIn} />
              <Route path="/register" component={SignIn} />
              <Route path="/books/:id" exact component={Details} />
              <Route path="/admin" component={Admin} />
              <Route
                path="/books/:bookId/reviews"
                exact
                component={CreateReviews}
              />
              <Route path="/books/:bookId/reviews/:reviewId" exact component={EditReviews} />
              <Route path="/donate" component={Donations} />
              <Route path="/AboutUs" component={AboutUs} />
              <Route path="/books" exact component={AllBooks} />
              {/* <Route path="/reviews" component={AllReviews} /> */}
            </Switch>
          </div>
        </div>
        </ReadingPointsContext.Provider>
      </UserContext.Provider>
    </BrowserRouter>
  );

  // return (
  //   <BrowserRouter>
  //     {/* <SidebarContext.Provider value={{ showSidebar, toggleSidebar }}> */}
  //     <Header />
  //     <UserContext.Provider value={{ user, setUser }}>
  //       <Navigation>
  //         <Switch>
  //           <Route path="/" exact component={Home} />
  //           <Route path="/login" component={SignIn} />
  //           <Route path="/register" component={SignIn} />
  //           {/* <Route path="/books/:bookId/reviews" exact component={CreateReviews} /> */}
  //           <Route path="/books" exact component={AllBooks} />
  //           {/* <Route path="/reviews" component={AllReviews} /> */}
  //         </Switch>
  //       </Navigation>
  //     </UserContext.Provider>
  //     {/* </SidebarContext.Provider> */}
  //   </BrowserRouter>
  // );
};

export default App;
