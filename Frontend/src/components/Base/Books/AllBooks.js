import React, { useState, useEffect, useContext } from "react";
import UserContext from "../../../providers/UserContext";
import { BASE_URL } from "../../../common/constants";
import SingleBook from "./SingleBook";
import "./SingleBook.css";
import "../Card.css";
import ReadingPointsContext from "../../../providers/ReadingPointsContext";

const AllBooks = (props) => {
  const [books, setBooks] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const {readingPoints, setReadingPoints } = useContext(ReadingPointsContext);

  useEffect(() => {
    setLoading(true);
    fetch(`${BASE_URL}/books`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
      },
    })
      .then((r) => r.json())
      .then((data) => {
        if (data.error) {
          throw new Error(data.message);
        }

        setBooks(data);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const borrowHandler = (bookId) => {
    fetch(`${BASE_URL}/books/${bookId}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
      },
    })
      .then((r) => r.json())
      .then((book) => {
        if (book.error) {
          throw new Error(book.message);
        }
      })
      .catch((error) => setError(error.message));
    const newBooks = [...books];
    newBooks.find((book) => book.id === bookId).isBorrowed = true;

    setBooks(newBooks);
  };

  const returnHandler = (bookId) => {
    fetch(`${BASE_URL}/books/${bookId}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
      },
    })
      .then((response) => response.json())
      .then((book) => {
        if (book.error) {
          throw new Error(book.message);
        }
      })
      .catch((error) => setError(error.message));
    const newBooks = [...books];
    newBooks.find((book) => book.id === bookId).isBorrowed = false;

    setBooks(newBooks);
    setReadingPoints(readingPoints+1);
  };

  return (
    <div className="cardsList">
      {loading ? <h1>LOADING...</h1> : null}
      {books.map((b) => (
        <SingleBook
          key={b.id}
          book={b}
          borrowHandler={borrowHandler}
          returnHandler={returnHandler}
          history={props.history}
          match={props.match}
        />
      ))}
      <hr />
    </div>
  );
};

export default AllBooks;
