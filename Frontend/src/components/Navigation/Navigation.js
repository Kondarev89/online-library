import React, { useContext } from "react";
import { NavLink, withRouter } from "react-router-dom";
import "./Navigation.css";
import userContext from "../../providers/UserContext";
import allBooks from "../Base/Books/AllBooks";
import Nav from "react-bootstrap/Nav";
import Button from "react-bootstrap/Button";
import { Row, Navbar } from "react-bootstrap";

const Navigation = (props) => {
  const { user, setUser } = useContext(userContext);
  const history = props.history;

  const logout = () => {
    setUser(null);
    localStorage.removeItem("token");
    history.push("/home");
  };

  return (
        <nav id="mainNav">

          {user ? (
            <div className="list-group">

          <Nav.Link href="/" className="list-group-item">
            Home
            </Nav.Link>
              <Nav.Link href="/books" className="list-group-item">
                All Books
              </Nav.Link>
              <Nav.Link href="/Donate" className="list-group-item">
                Donate
              </Nav.Link>
              <Nav.Link href="/About Us" className="list-group-item">
                About Us
              </Nav.Link>
              <Nav.Link href="/Contact Us" className="list-group-item">
                Contact Us
              </Nav.Link>
              <Button variant="dark" onClick={logout}>
                Logout
              </Button>
              {/* <Nav.Link to="/SingleBook" className="link">Single Book</Nav.Link> */}
              {/* <NavLink to="/profile" className="link">Profile</NavLink> */}
            </div>
          ) : (
            <>
              <Nav.Link href="/Donate" className="link">
                Donate
              </Nav.Link>
              <Nav.Link href="/About Us" className="link">
                About Us
              </Nav.Link>
              <Nav.Link href="/Contact Us" className="link">
                Contact Us
              </Nav.Link>
              <Nav.Link href="/register" className="link">
                Register
              </Nav.Link>
              <Nav.Link href="/login" className="link">
                Login
              </Nav.Link>
            </>
          )}
          {props.children}
        </nav>
  );
};

export default withRouter(Navigation);
