import React, { useState, useEffect,useContext } from 'react';
import { BASE_URL } from '../../../common/constants';
import AppError from '../../Pages/AppError/AppError';
import PropTypes from 'prop-types';
import UserContext, { getLoggedUser } from '../../../providers/UserContext';
    

const EditReviews = (props) => {


    const [error, setError] = useState(null);
    const bookId = props.match.params['bookId'];
    const reviewId = props.match.params['reviewId'];
    const [user, setUser] = useState(getLoggedUser());
    
    const [review, setReview] = useState({
        content: '',
    });
    const userContext = useContext(UserContext);
    const loggedUser = userContext.user;

    const updateReviewProp = (prop, value) => {
        review[prop]=value;
      }

    useEffect(() => {
        fetch(`${BASE_URL}/books/${bookId}/reviews`, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
          },
        })
          .then((r) => r.json())
          .then((r) => {
            if (r.error) {
              throw new Error(r.message);
            }
            
            setReview(r);
            
            
          })
          .catch((error) => setError(error.message));
      }, []);
      
    
    const updateReview = ({content}) => {
    
        
            fetch(`${BASE_URL}/books/${bookId}/reviews/${reviewId}`, {
              method: "PUT",
              headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
              },
              body: JSON.stringify({content}),
              
            })
            
              .then(r=> r.json())
              .then(r => {
                if (r.error) {
                  throw new Error(r.message);
                }
                const newReview = [...r];
                setReview(newReview)
                
        
                
              })
              .catch(error => setError(error.message));
              
          };



    return (
     <div>

        <div className="EditReview">
        <input type="text" onChange={e => updateReviewProp("content",e.target.value)} /><br /><br />
        <button onClick={() => updateReview(review)}>Edit review:</button>
        {/* <button className="button" onClick={updateReview}>Edit Review</button> */}
        <button className="button" onClick={() => props.history.goBack()}>Back</button>
    </div>
    </div>
    )

};

export default EditReviews;





