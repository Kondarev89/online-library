import React from "react";
import "./Header.css";
import { withRouter } from "react-router-dom";

const Header = ({ loggedUser, readingPoints, role, history }) => {
  return (
    <div className="header">
      <h1 className="headerTitle">Library</h1>
      <div className="userProfile">
        {loggedUser ? (
          <div>
            <p>User: {loggedUser}</p>
            <p>reading points: {readingPoints}</p>
          </div>
        ) : null}
      {role === "Administrator" ? (
        <button onClick={() => history.push("/admin")}>admin section</button>
      ) : null}
      </div>
    </div>
  );
};

export default withRouter(Header);
