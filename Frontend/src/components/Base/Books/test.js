
const rates = [{rate: 3},{rate: 4}]


const avgRating = (rates) => {
  const ratesLength = rates.length;
  const reducer = (accumulator, rate) => {
    return (accumulator + rate.rate)/ratesLength;
  };
  const ratesAvg = rates.reduce(reducer, 0);
  return ratesAvg;
}

console.log(avgRating(rates))