import { UsersAdminAController } from './users/users.admin.controller';
import { BooksController } from './books/books.controller';
import { UsersController } from './users/users.controller';
import { ReviewController } from './books/review.controller';
import { Module } from '@nestjs/common';
import { ServicesModule } from 'src/services/service.module';
import { AuthController } from './users/auth.controller';

@Module({
  imports: [ServicesModule],
  providers: [],
  controllers: [
    ReviewController,
    AuthController,
    UsersController,
    BooksController,
    UsersAdminAController,
  ],
})
export class ControllersModule {}
