import React, { useState } from 'react';
import { BASE_URL } from '../../../common/constants';

const CreateReviews = (props) => {
  const history = props.history;
  const bookId = props.match.params['bookId']
  const reviewId = props.match.params['reviewId']
  const [review, setReview] = useState({
    content:'',
  });
  
  const [error, setError] = useState(null);

  const updateReviewProp = (prop, value) => {
    review[prop]=value;
  }
  const addReview = ({content}) => {
    
// console.log(review)
    fetch(`${BASE_URL}/books/${bookId}/reviews`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
      },
      body: JSON.stringify({content}),
    })
      .then(r => r.json())
      .then(r => {
        if (r.error) {
          throw new Error(review.message);
        }
        const newReview = [...review];
        setReview(newReview)
        

        history.push('/reviews');
      })
      .catch(error => setError(error.message));
  };



  return (
    <div>
      <div>
      <h1>Create a review!</h1><br/>
      <label htmlFor="review-text">Review:</label>
      <input id="review-text" type="text" onChange={e => updateReviewProp("content",e.target.value)}></input><br/><br/>  
      <button onClick={() => addReview(review)}>Create review</button>
      <button onClick={() => history.goBack()}>Back</button>
      </div>
    </div>
  );
}

export default CreateReviews;
