import { Rate } from './rate.entity';
import { User } from './user.entity';
import { BorrowedBookLog } from './boroweBook.entity';
import { Review } from './review.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToMany,
  JoinTable,
} from 'typeorm';

@Entity('books')
export class Book {
  @PrimaryGeneratedColumn()
  id: number;
  @Column('nvarchar')
  title: string;

  @Column({ nullable:true })
  urlPoster: string | null;

  // TODO (optional): remove this property. We can check if the book is borrowed by looking for BorrowedBookLog with hasReturned = false
  @Column({ default: false })
  isBorrowed: boolean;

  @OneToMany(
    () => Review,
    review => review.book,
  )
  reviews: Review[];

  @OneToMany(
    () => BorrowedBookLog,
    borrow => borrow.book,
  )
  borrowedBooks: BorrowedBookLog[];

  @JoinTable()
  @ManyToMany(
    () => User,
    user => user.books,
  )
  users: User[];

  @OneToMany(
    () => Rate,
    rate => rate.book,
  )
  rates: Rate[];
}
