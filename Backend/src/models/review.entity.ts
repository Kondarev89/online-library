import { User } from './user.entity';
import { Book } from './book.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  OneToMany,
  OneToOne,
} from 'typeorm';
import { Expose } from 'class-transformer';
import { Like } from './likeDislike.entity';
import { Rate } from './rate.entity';

@Entity('reviews')
export class Review {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  content: string;

  @Column({ default: false })
  isDeleted: boolean;

  // @CreateDateColumn()
  // createOn: Date;

  // @UpdateDateColumn()
  // updateOn: Date;

  @ManyToOne(
    () => Book,
    book => book.reviews,
  )
  book: Book;

  @ManyToOne(
    () => User,
    user => user.reviews,
  )
  user: User;

  @OneToMany(
    () => Like,
    likes => likes.review,
  )
  likes: Like[];

  @OneToOne(
    () => Rate,
    rate => rate.review,
  )
  rate: Rate;

  @Expose()
  get transform() {
    return `ID: ${this.id}
    Content: ${this.content}`;
  }
}
