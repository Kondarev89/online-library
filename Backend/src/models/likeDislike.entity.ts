import { PrimaryGeneratedColumn, Entity, Column, ManyToOne } from 'typeorm';
import { Review } from './review.entity';
import { User } from './user.entity';

@Entity('likes')
export class Like {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: false })
  like: boolean;

  @ManyToOne(
    () => Review,
    Review => Review.likes,
  )
  review: Review;

  @ManyToOne(
    () => User,
    user => user.like,
  )
  user: User;
}
