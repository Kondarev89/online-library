import { Entity, ManyToOne, Column, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';
import { Book } from './book.entity';

@Entity('borrowedbooklogs')
export class BorrowedBookLog {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ default: false })
  hasReturned: boolean;
  @ManyToOne(
    () => Book,
    book => book.borrowedBooks,
  )
  book: Book;

  @ManyToOne(
    () => User,
    user => user.borrowedBooks,
  )
  user: User;
}
