import { UserRole } from './../enum/user.role';
import { ReturnReviewDto } from './../dtos/return.review.dto';
import { Injectable } from '@nestjs/common';
import { Review } from 'src/models/review.entity';
import { Book } from 'src/models/book.entity';
import { ReturnBookDTO } from 'src/dtos/return.book.dto';
import { User } from 'src/models/user.entity';
import { UserDTO } from 'src/dtos/user.dto';

@Injectable()
export class TransformServices {
  transformReview(review: Review): ReturnReviewDto {
    return {
      userName: review.user.username,
      bookTitle: review.book.title,
      content: review.content,
    };
  }

  transformBook(book: Book) {
    return {
      id: book.id,
      title: book.title,
      isBorrowed: book.isBorrowed,
    };
  }

  transformBorrowBook(book: Book): ReturnBookDTO {
    return {
      id: book.id,
      title: book.title,
      isBorrowed: book.isBorrowed,
      // urlPoster: book.urlPoster,
    };
  }
  transformUser(user: User): UserDTO {
    return {
      id: user.id,
      username: user.username,
      role: UserRole[user.role],
      isDeleted: user.isDeleted,
      
    };
  }
}
