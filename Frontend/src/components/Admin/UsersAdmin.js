import React, { useState, useEffect } from "react";
import "../../App.css";
import { BASE_URL } from "../../common/constants";

const UsersAdmin = () => {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  useEffect(() => {
    setLoading(true);
    fetch(`${BASE_URL}/admin/users`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
      },
    })
      .then((r) => r.json())
      .then((data) => {
        if (data.error) {
          throw new Error(data.message);
        }

        setUsers(data);
      })
      .catch((error) => setError(error.message))
      .finally(() => setLoading(false));
  }, []);

  const deleteUser = (userId) => {
    fetch(`${BASE_URL}/admin/users/${userId}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
      },
    })
      .then((r) => r.json())
      .then((r) => {
        if (r.error) {
          throw new Error(r.message);
        }
      })
      .catch((error) => setError(error.message));
    setUsers(users.filter((user) => user.id !== userId))
  };

  return (
    <div className="cardList">
      {loading ? (
        <div>Loading...</div>
      ) : (
        users.map((user) =>
          !user.isDeleted ? (
            <div key={user.id} className="card userCard">
              <div className="userDetails">
                <p>ID: {user.id}</p>
                <div>User: {user.username}</div>
                <div>Role: {user.role}</div>
              </div>
              <div className="userActions">
                <input
                  type="button"
                  value="Delete"
                  onClick={() => deleteUser(user.id)}
                />
              </div>
            </div>
          ) : null
        )
      )}
    </div>
  );
};

export default UsersAdmin;
