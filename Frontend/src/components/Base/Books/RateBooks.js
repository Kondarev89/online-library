import React, { useState } from "react";
import { BASE_URL } from "../../../common/constants";
import "./SingleBook.css";


const RateBooks = ({ bookId, rate }) => {
  const [bookRating, setRate] = useState(rate);
  const [error, setError] = useState(null);


  const rateBook = (bookId, e) => {
    setRate(e);

    fetch(`${BASE_URL}/books/${bookId}/rates`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json, text/plain, */*",
        Authorization: `Bearer ${localStorage.getItem("token") || ""}`,
      },

      body: JSON.stringify({ rate: e }),
    })
          .then((response) => response.json())
          .then((data) => {
              if (data.error) {
                  throw new Error(data.message);
              }
          })
          .catch((error) => setError(error.message));

  };

  return (
    <div className="bookRating">
      <h2>Rate book</h2>
      {[1, 2, 3, 4, 5].map((e, i) => (
        <div
          key={i}
          className={bookRating >= e ? "fa fa-star checked" : "fa fa-star"}
          onClick={() => rateBook(bookId, e)}
        ></div>
      ))}
      {error ? <div><p>{error}</p><button  onClick={() => setError(false)}>Return and try again</button></div> : null}
    </div>
  );
};
export default RateBooks;
