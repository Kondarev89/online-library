import { UserDTO } from 'src/dtos/user.dto';
import { BooksService } from './../../services/books/books.service';
import { UsersService } from './../../services/users/users.services';
import {
  Controller,
  Delete,
  Param,
  Get,
  UseGuards,
  Query,
  Post,
  Body,
} from '@nestjs/common';
import { User } from 'src/models/user.entity';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/enum/user.role';
import { ReturnBookDTO } from 'src/dtos/return.book.dto';
import { CreateBookDTO } from 'src/dtos/create.book.dto';

@Controller('admin')
export class UsersAdminAController {
  constructor(
    private readonly userService: UsersService,
    private readonly bookService: BooksService,
    
  ) {}

  @Get('users')
  @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  async getAll(): Promise<UserDTO[]> {
    return this.userService.getAllUsers();
  }

  @Get('users/:userId')
  @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  async getUserById(@Param('userId') userId): Promise<User> {
    return this.userService.getUserById(userId);
  }

  @Delete('/users/:userId')
  @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  async delete(@Param('userId') userId) {
    return await this.userService.delete(userId);
  }

  @Get('/books')
  @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  async allBooks(
    @Query('pageSize') pageSize: number,
    @Query('page') page: number,
  ): Promise<ReturnBookDTO[]> {
    const allBooks = await this.bookService.getAllBooks(pageSize, page);
    return allBooks;
  }

  @Post('/books')
  @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  async create(@Body() book: CreateBookDTO) {
    return this.bookService.createBook(book.title);
  }

  @Delete('/books/:bookId')
  @UseGuards(AuthGuard('jwt'), new RolesGuard(UserRole.Administrator))
  async deleteBook(@Param('bookId') bookId) {
    return this.bookService.deleteBook(bookId);
  }
}
