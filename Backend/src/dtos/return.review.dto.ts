export class ReturnReviewDto {
  content: string;
  bookTitle: string;
  userName: string;
}
