export class UserDTO {
  id: number;
  username: string;

  role: string;
  isDeleted: boolean;

}
