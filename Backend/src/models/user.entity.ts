import { Rate } from './rate.entity';
import { UserRole } from './../enum/user.role';
import { BorrowedBookLog } from 'src/models/boroweBook.entity';
import { Book } from 'src/models/book.entity';
import { Review } from './review.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToMany,
} from 'typeorm';
import { Like } from './likeDislike.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  username: string;

  @Column()
  password: string;

  @Column({
    type: 'enum',
    enum: UserRole,
    default: UserRole.Basic,
  })
  role: UserRole;

  @Column({ default: false })
  isDeleted: boolean;

  @OneToMany(
    () => Review,
    review => review.user,
  )
  reviews: Review[];

  @ManyToMany(
    () => Book,
    book => book.users,
  )
  books: Book[];

  @OneToMany(
    () => Like,
    like => like.user,
  )
  like: Like;

  @OneToMany(
    () => BorrowedBookLog,
    borrow => borrow.user,
  )
  borrowedBooks: BorrowedBookLog[];

  @ManyToMany(
    () => Rate,
    rate => rate.user,
  )
  rates: Rate[];

  @Column({ default: 0 })
  readingPoints: number;

  @Column({
    nullable: true,
  })
  banEndDate: Date;
}
