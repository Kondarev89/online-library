export class ReturnBookDTO {
  id: number;
  title: string;
  isBorrowed: boolean;
  // urlPoster: string | null;
}
