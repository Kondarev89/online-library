import { IsNumber, IsNotEmpty } from 'class-validator';

export class CreateRateDTO {
  @IsNumber()
  @IsNotEmpty()
  rate: number;
}
